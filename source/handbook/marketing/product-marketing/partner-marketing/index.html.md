---
layout: markdown_page
title: "Partner marketing"
---

Welcome to the Partner Marketing Handbook

[Up one level to the Product Marketing Handbook](/handbook/marketing/product-marketing/)    

## On this page
* [Partner Marketing Objectives](#partner-marketing-objectives)
* [Partner Marketing Activation](#partner-marketing-activation)
* [Partner Newsletter Feature](#partner-newsletter)

## Partner Marketing Objectives<a name="partner-marketing-objectives"></a>

- Promote existing partnerships to be at top-of-mind for developers
- Integrate resale partnerships: promote partnership integrations/products which we sell as part of our sales process.
- Migrate open source projects to adopt GitLab, and convert their users in-turn to GitLab.
- Surface the ease of GitLab integration to encourage more companies to integrate with GitLab.
- Build closer relationships with existing partners through consistent communication

[Strategic Partner listing](https://docs.google.com/document/d/1-oAf0tMlTrAaPAsG_8NLXrI3DEZqI5ZA0gW0lKxFjA4/edit) (internal)

## Partner Marketing Activation<a name="partner-marketing-activation"></a>

Within first 2 weeks of partnership agreement, generate initial buzz by:

- Blog post announcing partnership. To be published on website, promoted on both companies social media platforms, and included in company newsletter, & partner newsletter
- Add partner to our website partner page. Request that GitLab is added to partner's website
- Add partner email address to partner newsletter list in Marketo

Tier out marketing efforts over next few months to ensure that there is consistent communication to the market and target audience to keep partnership front-of-mind. Suggested ongoing marketing activities:

- Webinar with partner - Host, and share leads.
- Webinar with partner and customer
- Customer case study
- How to video/demo 
- Attend events together, speak at each-other's meet-ups
- Discount for their service and send out to all our customers

## Partner Newsletter Feature<a name="partner-newsletter"></a>

In line with the objective of: Promote existing partnerships to be at top-of-mind for developers, a regular feature in our fortnightly (8th & 22nd) newsletter will promote our partners to our target audience. This feature should be co-authored by the partner.

Possible content:

- Feature on new partner if signed
- Feature on existing partner if major update released
- Feature on existing partner highlighting benefits of partner product
- 1-minute video showcasing the integration as a reference
- Blog post from partner
- Feature on how existing customer uses GitLab and partner

Suggested Format:

- Length: Couple of paragraphs
- Links: GitLab integration/partner page & partner website
- ROI - Track click-through to partner website and provide partner with metrics

Submit to the 8th and 22nd of the month newsletter issue in the marketing project. Request that partner features segment in their newsletter if possible.
