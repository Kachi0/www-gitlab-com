---
layout: markdown_page
title: "People Operations"
---

## Communication

- [**Public Issue Tracker**](https://gitlab.com/gitlab-com/peopleops/issues/); please use confidential issues for topics that should only be visible to team members at GitLab)
- You can also send an email to the People Operations group (see the "GitLab Email Forwarding" google doc for the alias), or ping an individual member of the People Operations team, as listed on our [Team page](https://about.gitlab.com/team/).
- [**Chat channel**](https://gitlab.slack.com/archives/peopleops); please use the `#peopleops` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

## Other pages related to People Operations

- [Hiring process](/handbook/hiring/)
- [Global Compensation Framework](/handbook/people-operations/global-compensation-framework/)
- [Onboarding](/handbook/general-onboarding/)
- [Underperformance](/handbook/underperformance)
- [Offboarding](/handbook/offboarding/)
- [Benefits](/handbook/benefits/)
- [Travel](/handbook/travel/)
- [People Operations Standard Operating Procedures (SOPs)](/handbook/people-operations/sop/)

## On this page
{:.no_toc}

- TOC
{:toc}


## Role of People Operations
{: #role-peopleops}

In general, the People Operations team and processes are here as a service to the rest of the team; helping make your life easier so that you can focus on your work and contributions to GitLab. On that note, please don't hesitate to [reach out](https://about.gitlab.com/handbook/people-operations/#communication) with questions! In the case of a conflict between the company and a team member, People Operations works "on behalf of" the company.

## Team Directory
{: #directory}

The [team directory](https://gitlab.bamboohr.com/employees/directory.php?pin) is in BambooHR, and is accessible to all team members. This is your one-stop directory for phone numbers and addresses (in case you want to send your team mate an awesome card!).

- Please make sure that your own information stays up to date, and reach out to People Ops if you need any help in doing so.
- Please make sure that your address and phone information are written in such a way that your team mates can reach you from a different country. So, for example, include `+[country code]` in front of your phone number.

## Letter of Employment

If you need a letter from GitLab verifying your employment/contractor status, please send the request to People Ops citing what information is needed. For example, monthly fee, salary, start date, title, etc. People ops will send you the letter once it is completed.

## Office addresses
{: #addresses}

- For the SF office, see our [visiting](https://about.gitlab.com/visiting/) page.
- For the NL office, we use a postbox address listed in the "GitLab BV address" note in the Shared vault on 1Password. We use [addpost](www.addpost.nl) to scan our mail and send it along to a physical address upon request. The scans are sent via email to the email alias listed in the "GitLab Email Forwarding" google doc.

## Regular compensation

1. Employees of our Dutch entity (GitLab B.V.) will get their salary wired on the
25th of every month, and can see their pay slip in their personal portal on
[HR Savvy's system](https://hr-savvy.nmbrs.nl/) towards the end of the month.
1. Employees of our US entity (GitLab Inc.) have payroll processed semi-monthly
through TriNet, and they can access their pay slips through the [TriNet portal](https://www.hrpassport.com).
1. Contractors to GitLab (either entity) should send their invoices for services rendered to ap@gitlab.com
   - For 'fixed fee' contracts, it is OK to send the invoice before the time period
   that it covers is over. For example, an invoice covering the period of March 1-31 can be sent on March 25.
   - All invoices are internally reviewed, approved, and then payment is processed.
   This is usually a fast process, but be aware that it can incur delays around vacations. In principal, payments go out once per week on Fridays.
   - An invoice template can be found as a Google sheet named "Invoice Template" (also listed on the [finance page](/handbook/finance/) )
1. To process any changes, see the directions on the [people-operations page](/handbook/people-operations/sop/#processing-changes).

## Policies

### Sick time - taking and reporting
{: #sick-time}

In keeping with our [values](/handbook/#values) of freedom, efficiency, transparency, kindness, and boring solutions, we have crafted the following protocol around sick leave for all team members.

**All team members**

- If you or a loved one is ill, we want you to take care of yourself or your loved one(s). To facilitate this, you should take sick leave when you need it. Sick leave is meant to be used when you are ill, or to care for family members including your parent(s), child(ren), spouse, registered domestic partner, grandparent(s), grandchild(ren), and sibling(s).
- You do need to report when you take sick leave, either by emailing your manager and People Ops, or by using the "Request time off" function in BambooHR. This way, it can be tracked in BambooHR and related payroll systems.
- If you need sick leave for more than 8 consecutive calendar days, notify your manager and People Ops to accommodate an extended leave request. What can (or must) be accommodated varies from location to location: GitLab will comply with the applicable laws in your specific location.
- Upon request, you should be able to provide proper documentation of the reason for your sick leave (doctor's note).

**Details for specific groups of team members**

- Employees of GitLab Inc. who receive a pay stub from TriNet will see sick time accrue on their pay stub at the rate of 0.0346 hrs per hour worked (3 hours of sick leave per semi-monthly pay-period) for a maximum accrual and carry-over of 72 hours per year. GitLab's policy is more generous than this, in the sense that you can take off non-accrued sick time as written above (a negative balance may show on your pay stub). Sick time does not get paid out in case of termination, nor does it reduce your final paycheck in case of a negative balance. Related to the topic of extended leave requests, see information about [short term disability](/handbook/benefits/#std-ltd) through TriNet / your state.
- Employees of GitLab B.V. have further rights and responsibilities regarding sick time based on Dutch law, as written into their employment [contracts](https://about.gitlab.com/handbook/contracts/#employee-contractor-agreements).

### Hiring Significant Other or Family Members

GitLab is committed to a policy of employment and advancement based on **qualifications and merit** and does not discriminate in favor of or in opposition to the employment of significant others or relatives. Due to the potential for perceived or actual conflicts, such as favoritism or personal conflicts from outside the work environment, which can be carried into the daily working relationship, GitLab will hire or consider other employment actions concerning significant others and/or relatives of persons currently employed or contracted only if:   
a) candidates for employment will not be working directly for or supervising a significant other or relative, and
b) candidates for employment will not occupy a position in the same line of authority in which employees can initiate or participate in decisions involving a direct benefit to the significant other or relative. Such decisions include hiring, retention, transfer, promotion, wages and leave requests.

This policy applies to all current employees and candidates for employment.

### Relocation

If you change your address, you should let GitLab know the new address within a month. The best way to do this is by logging in to BambooHR and changing your address under the **Personal** tab. This triggers a message to the BambooHR admin to review the change and "accept" it.

- PeopleOps will check that any necessary changes to payroll and benefits administration are processed in time.
- If your relocation is to a different metro area, then to stay aligned with our [compensation principles](/handbook/people-operations/global-compensation-framework/#compensation-principles) and per the [standard contract agreements](/handbook/contracts), you should obtain written agreement first (from your hiring manager) that your current contract continues to be valid "as-is" or if it needs to be adjusted with respect to compensation or other terms. Refer to the ["move calculator"](https://about.gitlab.com/jobs/move) to see what the likely impact will be. People Ops will process any changes that are agreed on, and file the email in BambooHR.

### Administrative details of benefits for US-based employees
{: #benefits-us}

#### 401k

1. You are eligible to participate in GitLab’s 401k as of the 1st of the month after your hire date.
1. You will receive a notification on your homepage in TriNet Passport once eligible,
if you follow the prompts it will take you to the Transamerica website https://www.ta-retirement.com/
or skip logging in to TriNet Passport and go directly to https://www.ta-retirement.com/
after the 1st of the month after your hire date.
1. Once on the home page of https://www.ta-retirement.com/ go to "First Time User Register Here".
1. You will be prompted for the following information
   1. Full Name
   1. Social Security Number
   1. Date of Birth
   1. Zip Code
1. Once inside the portal you may elect your annual/pay-period contributions, and Investments.

### Using RingCentral
{: #ringcentral}

Our company and office phone lines are handled via RingCentral. The login credentials
are in the Secretarial vault on 1Password. To add a number to the call handling & forwarding
rules:

- From the Admin Portal, click on the Users button (menu on left), select the user for which you
want to make changes (for door handling, pick extension 101).
- A menu appears to the right of the selected user; pick "Call Handling & Forwarding" and review
the current settings which show all the people and numbers that are alerted when the listed User's
number is dialed.
- Add the new forwarding number (along with a name for the number), and click Save.


## Dutch work permits

Some of our GitLab team members in the Netherlands have a "werkvergunning" or work permit under the [highly skilled migrants](https://ind.nl/EN/business/employer/highly-skilled-migrants/Pages/default.aspx) category of the Immigration and Naturalization Service (IND).

- GitLab is a recognized organization ("erkend referrent") with the IND, and Savvy provides support with respect to applying for new visas / permits or extending existing ones.
- Work permits must be renewed at the end of each contract period, but at minimum once every 5 years.
- At the time of applying for permit renewal, the application must satisfy various criteria including an age-dependent [minimum salary requirement](https://www.ind.nl/en/individuals/employee/costs-income-requirements/Income-requirements) (with a step at age 30). This requirement should be taken into consideration when issuing a new contract, since the contract can be made valid for just a year or for an indefinite period; thus triggering more or less frequent re-applications for work permit extensions.

Here is a [generally helpful guide](http://www.expatica.com/nl/visas-and-permits/When-your-residence-permit-expires-or-you-want-to-leave-the-Netherlands_108416.html) on considerations around permit extensions.


## Paperwork people may need to obtain mortgage in the Netherlands
{: #dutch-mortgage}

When your employment contract is for a fixed period of time (e.g. 12 months) you'll need a "werkgeversverklaring".
This document describes your salary and states that your employer expects to continue to employ
you after the contract expires (assuming the performance of the employee doesn't degrade).
This document has to be filled in by hand, preferably using blue ink, and must be signed
and stamped. If there is no stamp (as is the case for GitLab) an extra letter (also signed)
must be supplied that states the employer has no stamp. While the language of these
documents doesn't matter, the use of Dutch is preferred.

Employees also have to provide a copy of a payslip that clearly states not only their
monthly salary but also their annual salary. These numbers must match the numbers on
the "werkgeversverklaring" down to the decimals. Mortgage providers may also require
you to provide information about your financial status, usually in the form of a report/screenshot
of your total financial status (including any savings you have). The requirements for
this seem to vary a bit between mortgage providers.

## Involuntary Terminations

Involuntary termination of any team member is never easy. We've created some guidelines and information to make this process as painless and easy as possible for everyone involved. Beyond the points outlined below, make sure to refer to our guidelines on [underperformance](/handbook/underperformance), as well as the [offboarding](/handbook/offboarding/) checklist.

### Overall process

Ideally, the manager and the team member have walked through the guidelines on [underperformance](/handbook/underperformance) before reaching this point.

1. Manager: reach out to People Operations for assistance. People Ops will ask about what the performance issues have been, how they have been attempted to be addressed, and will then prepare for the termination.
1. Manager and People Ops: discuss best mode of communicating the bad news to the team member. This discussion can happen via a private chat-channel, but it is best to be done via a video hangout. Set up a private chat-channel in any case, since this is also useful to have during the eventual call with the affected team member.
1. Manager and People Ops: Decide who will handle which part of the conversation, and if desired, practice it. It is strongly advised to have someone from People Ops on the call when the bad news is delivered.
1. Manager and People Ops: Decide what offboarding actions need to be taken _before_ the call (e.g. revoke admin permissions), or _during_ the call (e.g. revoke Slack and Gmail access), and which ones can wait until later (see the [offboarding checklist](/handbook/offboarding)). Make sure someone with the necessary admin privileges is on hand (in the private chat-channel) to assist with those sensitive offboarding steps that should occur relatively quickly. Do not create the offboarding issue until _after_ the call, since even confidential issues are still visible to anyone in the team.
1. Manager: Set up a call with the team member in question. Make a separate private calendar event to invite the People Ops representative.
1. On the call: deliver the bad news up-front, do not beat around the bush and prolong the inevitable pain for everyone involved. A sample leading sentence can be "Thanks for joining the call ___ . Unfortunately, the reason I wanted to speak with you, is because we have decided that we have to let you go and end your employment / contract with GitLab." At this point, hand over the call to People Ops to continue. People Ops explains what led to this decision and points to the process that was followed to reach this decision. People Ops to make it clear that the decision is final, but also to genuinely listen to their side of the story since there may be useful lessons in what they say for the rest of the team e.g. regarding hiring and vetting practices.
1. People Ops: Make sure to communicate the [practical points](#offboarding-points) from the termination memo outlined below.
1. People Ops: Create the [offboarding checklist issue](/handbook/offboarding), and go from there.


### Points to cover during the offboarding call, with sample wording
{: #offboarding-points}

The following points need to be covered for any team member:

1. Final Pay: "your final check (or invoice period) is for the pay period of X and includes X days of pay”.
1. Company Property: “please return all property as explained in the handbook, also please delete GitLab’s email connection from your phone”.
1. Business Expenses: “please create your final expense report to Expensify (for employees), OR, please file any outstanding expenses with your final invoice (for contractors), so these can be reimbursed to you in a timely manner”.
1. Confidentiality and Non-Disclosure: “we know you are a professional, please keep in mind the agreement you signed when you were hired”.

The following points need to be covered for US-based employees:

1. COBRA: “your benefits will cease on last day of the month you are eligible for Consolidated Omnibus Budget Reconciliation Act (“COBRA”), the carrier (TriNet) has been notified and the carrier will send out the paperwork to your home address on file”.
1. PPACA: "You may also be eligible under the Patient Protection and Affordable Care Act (“PPACA”) for subsidized health care options via the marketplace. If you are interested it is important that you sign up with the market place well before the 15th of the month to have coverage for the following month”.
1. HIPAA: " under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), if you need a certificate of credible coverage please download it from your current carrier's online portal or request it from TriNet”.
1. Unemployment insurance: "it is up to your state's labor agency (in CA: EDD) to decide if you are eligible for unemployment insurance”.
1. Please remember to keep TriNet informed if you move I want to be sure your W-2 gets to you at the end of the year. You may also contact X at GitLab (provide phone number and email address) with any other questions that you may have" (consider inviting them to contact you at anytime for any reason)

### Sample termination memo

If appropriate (to be determined by conversation with the manager, CEO, and people ops), use the following [termination memo](https://docs.google.com/document/d/11Uk8p4VJrLnDD5IAtbTwswPUUEnmeEOazS1kJMhOu70/edit?usp=sharing), which is provided here as an openly viewable Google Doc, but of course needs to be personalized and tailored to each individual's situation. As written, it is applicable to US-based employees only.
